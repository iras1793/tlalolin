<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');

class App extends CI_Controller {


	public function index($value=''){
		$this->load->view('welcome_message');
	}

	public function addMensaje($value=''){
		$respuesta['success']   = 0;
		$respuesta['extra']     = '';
		$respuesta['random_id'] = '';
		$fields = array();
		
		$this->form_validation->set_rules('contacto', 'Contacto', 'trim|required');
		$this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
		$this->form_validation->set_rules('observaciones', 'Observaciones', 'trim|required');
		$this->form_validation->set_rules('latitud', 'Latitud', 'trim|required');
		$this->form_validation->set_rules('longitud', 'Longitud', 'trim|required');
		if($this->form_validation->run() == TRUE){
			$fields['observaciones']['value'] = $this->security->xss_clean($this->input->post('observaciones',true));
			$fields['contacto']['value']      = $this->security->xss_clean($this->input->post('contacto',true));
			$fields['telefono']['value']      = $this->security->xss_clean($this->input->post('telefono',true));
			$fields['latitud']['value']       = $this->security->xss_clean($this->input->post('latitud',true));
			$fields['longitud']['value']      = $this->security->xss_clean($this->input->post('longitud',true));
			$fields['foto']['value']          = $this->validImg('file');
			//genera el string aleatorio
			$random = _RandomString();
			while( $this->erian->duplicadosID($random) ){//verifica que sea unico dentro de la base
				$random = _RandomString();
			}
			$fields['random_id']['value'] = $random;
			$result = $this->erian->add($fields);
			if ($result) {
				$respuesta['success']   = 1;
				$respuesta['extra']     = "Registro exitoso";
				$respuesta['random_id'] = $fields['random_id']['value'];
			}else
				$respuesta['extra'] = 'Error en agregar registro';
		}else{
			log_message('info','=====>No pasa validaciones');
		}

		echo json_encode($respuesta);
	}//end function


	public function getInfo($filtro=''){
		$respuesta['success'] = 0;
		$respuesta['extra']   = '';
		
		$result = $this->erian->fetch();
		if ($result) {
			$respuesta['extra'] = $result;
			$respuesta['success'] = 1;
		}

		echo json_encode($respuesta);
	}


	public function validImg($name=''){
		if(isset($_FILES[$name])){
			if($_FILES[$name]['error']!=0){
				log_message('info','======>No hay imagen de :'.$name);
				return '';
			}elseif( $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
	        	$extension = pathinfo($_FILES[$name]['name'],PATHINFO_EXTENSION);
	        	$file_name = $_FILES[$name]['name'] = 'IMG_'.time().'.'.$extension;
	        	if( _verificaImagen($extension,$_FILES[$name]['size'])){
	            	//preparamos el destino de la imagen
	            	$destino= "./img/".$file_name;
		            if (move_uploaded_file($_FILES[$name]['tmp_name'], $destino)){
		                log_message("INFO","--->validTeaser---> La imagen  se subio");
		                return $file_name;
		            }else
		                log_message('info','=====>Problemas al subir imagen'); return '';
		        }else
	                log_message('info','=====>Problemas al validar imagen'); return '';
			}
		}else
			return '';
	}//end function

}