<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Controlador Administradores - Tlalolin
*/
class Inicio extends CI_Controller{
	var $data = array();

	var $fields = array(
			'alias' => array('value'=>'','validate'=> array(
													'label' => 'Alias',
													'rules' => 'trim|required|xss_clean'
								)),
			'nombre' => array('value'=>'','validate'=> array(
													'label' => 'Nombre',
													'rules' => 'trim|required|xss_clean'
								)),
			'tipo' => array('value'=>'','validate'=> array(
													'label' => 'Tipo',
													'rules' => 'trim|required|xss_clean'
								)),
			'password' => array('value'=>'','validate'=> array(
													'label' => 'Contraseña',
													'rules' => 'trim|required|xss_clean'
								))
		);

	var $tipos = array('1'=>'Administrador','2'=>'Usuario');

	function __construct(){
		parent::__construct();
		$this->erian->table = 'tab_users';
		$this->erian->id = 'id';
		if( !$this->auth->loggedin() )
			redirect('admin/login');

		$this->data['user_type'] = $this->auth->usertype();

	}//end construct

	public function index(){
		$config['base_url']         = site_url('admin/inicio/pagina/');
		$config['total_rows']       = $this->erian->countRecords();
		$config['per_page']         =  10;
		$config['cur_tag_open']     = '<a class="active">';
		$config['cur_tag_close']    = '</a>';
		$config['first_link']       = "Primero";
		$config['last_link']        = "Último";
		$config['next_link']        = 'Siguiente';
		$config['prev_link']        = 'Anterior';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		
		$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],1,'id,nombre,alias,tipo',null,null,'id');
		$this->data['links']   = $this->pagination->create_links();

		$this->template->content->view('admin/usuarios',$this->data);
		$this->template->publish('admin/template_admin');
	}

	/**
	 * Funcion encargada de llevar la paginación
	 * @param  integer $pagina 
	 * @return [type]          [description]
	 */
	public function pagina( $pagina = 1 ){
		
		$pag = $pagina;
		//arreglo de configuacion para uso de paginacion
		$config['base_url']         = site_url('admin/inicio/pagina/');
		$config['total_rows']       = $this->erian->countRecords();
		$config['per_page']         =  10;
		$config['cur_tag_open']     = '<a class="active">';
		$config['cur_tag_close']    = '</a>';
		$config['first_link']       = "Primero";
		$config['last_link']        = "Último";
		$config['next_link']        = 'Siguiente';
		$config['prev_link']        = 'Anterior';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);		
		
		$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'id,nombre,alias,tipo',null,null,'id');
		$this->data['links']   = $this->pagination->create_links();
		
		$this->template->title = "Restaurantes";
		$this->template->content->view('admin/usuarios',$this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	/*
	*Funcion encargada de agregar un nuevo registro
	*return mensaje de exito o fallo
	*/
	public function agregar(){
		$this->data['title'] = "";
		$this->data['id']    = '';
		$this->data['action'] = 'agregar';
		$this->config_validates();
		
		if ($this->form_validation->run() != FALSE) {
			$this->fields['password']['value'] = md5($this->fields['password']['value']);
			$result = $this->erian->add($this->fields);
			if ($result!= false) {
				$this->messages->add("Se ha agregado el registro de manera correcta","success");
				foreach ($this->fields as $key => $value)
					$this->fields[$key]['value'] = null;
			}else
				$this->messages->add("No se pudo guardar la información","error");
		}
		$this->data['fields'] = $this->fields;
		
		$this->data['titulo'] = "Agregar nuevo usuario";
		$this->template->title = "";
		$this->data['tipos'] = $this->tipos;
		$this->template->content->view('admin/usuarios_form',$this->data);
		$this->template->publish('admin/template_admin');
	}//end function

	/**
	* Funcion encargada de eliminar un registro
	* @param  int $id identificar a eliminar
	* @return boolean     true on succes and a message
	*/
	public function eliminar($id=''){
		if (!is_numeric($id))
			redirect('admin/inicio');
		
		
		$res = $this->erian->delete($id);
		if ($res!=false) 
			$this->messages->add('El registro ha sido eliminado','success');
		else
			$this->messages->add('Hubo un error al eliminar el registro, intentelo más tarde.','error');
		
		redirect('admin/inicio');
	}//end function	

	/**
	* Funcion encargada de editar un registro
	* @param  int $id  id del registro
	* @return boolean     true on success and a message
	*/
	public function editar($id=''){
		if (!is_numeric($id))
			redirect('admin/inicio');
		
		$this->data['title'] = "";
		$this->data['action'] = "editar";
		
		$info = $this->erian->get(array('id'=>$id));
		
		$this->config_validates();
		if ($this->form_validation->run() != false) {
			$pwd = md5($this->fields['password']['value']);
			if( strcmp($info[0]->password,$pwd) == 0 )
				unset($this->fields['password']);

			if(empty($this->fields['password']['value'])) 
				unset($this->fields['password']);

			$this->fields['password']['value'] = $pwd;
			$result = $this->erian->update($this->fields,$id);
			if ($result!=false) 
				$this->messages->add("Se ha actualizado el registro de manera correcta.","success");	
			else
				$this->messages->add("No se pudo guardar la informacion, vuelva a intentarlo.","error");	
		}
		
		foreach($this->fields as $key => $value)
			$this->fields[$key]['value'] = $info[0]->$key;
		
		$this->fields['password']['value'] = '';
		$this->data['fields']  = $this->fields;
		$this->data['id']      = $id;
		$this->data['titulo']  = "Editar usuario";
		$this->template->title = "";
		$this->data['tipos'] = $this->tipos;
		$this->template->content->view('admin/usuarios_form',$this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	/**
	* Funcion encargada de validar las reglas para el formulario
	*/
	public function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->security->xss_clean($this->input->post($key,true));
	}//end function


}//end class