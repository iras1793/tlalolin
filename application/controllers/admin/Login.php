<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Controlador de tlalolin
*   Administrador de usuarios
*/
class Login extends CI_Controller {

	var $data = array();
	
	var $fields = array(
			'alias' => array('value'=>'','validate'=> array(
													'label' => 'Alias',
													'rules' => 'trim|required|xss_clean'
								)),
			'nombre' => array('value'=>'','validate'=> array(
													'label' => 'Nombre',
													'rules' => 'trim|xss_clean'
								)),
			'pwd' => array('value'=>'','validate'=> array(
													'label' => 'Contraseña',
													'rules' => 'trim|required|xss_clean'
								))
		);


	function __construct(){
	  parent::__construct();
		
		$this->erian->table = 'tab_users';
		$this->erian->id    = 'id';

/*		else
			redirect("admin/inicio");*/
	}

	/**
	 * Funcion encargada de validar la sesion del usuario, si esta logeado redirecciona al inicio
	 * del CRM
	 */
	public function index(){
		$this->data['user_type'] = $this->auth->usertype();
		if( $this->auth->loggedin() )
			redirect("admin/inicio");
		$this->template->content->view('admin/login',$this->data);
		$this->template->publish('admin/template_login');
	}


	/**
	 * Funcion encargada de recibir los datos del usuario, validar usuario y coincidencia de contraseña
	 */
	public function startSession(){
		$this->config_validates();
		if ( $this->form_validation->run() != FALSE ) {
			$usuario['nombre']  = $this->fields['nombre']['value'];
			$usuario['pwd']     = md5($this->fields['pwd']['value']);
			$usuario['alias']   = $this->fields['alias']['value'];
			$user = $this->erian->getLogin($usuario);
			if ($user) {
				$this->auth->login($user,false);
				redirect('admin/inicio');
			}else{
				$this->messages->add("El alias o contraseña son incorrectos.","error");
				redirect('admin');
			}
		}else
			redirect('admin');
	}//end function


	/**
	 * Funcion encargada de cerrar la sesion del usuario
	 */
	public function logout(){
		log_message('info','====> LLego a logut');
		$this->auth->logout();
		redirect('admin');
	}

	/**
	* Funcion encargada de validar las reglas para el formulario
	*/
	public function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->security->xss_clean($this->input->post($key,true));
	}//end function



}//end class
