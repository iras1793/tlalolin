<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	var $data = array();

	function __construct($foo = null){
		parent::__construct();
		$this->data['pagina_pendiente'] = 1;
		$this->data['pagina_revisado']  = 1;
		$this->data['pagina_revision']  = 1;
		$this->data['pagina_urgente']   = 1;
	}

	public function index(){
		//arreglo de configuacion para uso de paginacion
		$config['base_url']         = site_url('inicio/pagina/');

		$config['total_rows']       = $this->erian->countRecords();
		$config['per_page']         =  10;
		$config['cur_tag_open']     = '<a class="active">';
		$config['cur_tag_close']    = '</a>';
		$config['first_link']       = "Primero";
		$config['last_link']        = "Último";
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		$this->data['links']  = $this->pagination->create_links();
		$this->data['pagina_pendiente'] = 1;
		$this->data['pagina_revisado']  = 1;
		$this->data['pagina_revision']  = 1;
		$this->data['pagina_urgente']   = 1;
		//$this->data['results'] = $this->erian->fetch();
		$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],1,'*',null,null,'id');
		$this->template->title = "Brigada » ayuda";
		$this->data['estados'] = _estados();
		$this->template->content->view('inicio',$this->data);
		$this->template->publish('template_site');
	}//end function



	public function pagina($pagina=1){
		//arreglo de configuacion para uso de paginacion
		$config['base_url']         = site_url('inicio/pagina/');
		$config['total_rows']       = $this->erian->countRecords();
		$config['per_page']         =  10;
		$config['cur_tag_open']     = '<a class="active">';
		$config['cur_tag_close']    = '</a>';
		$config['first_link']       = "Primero";
		$config['last_link']        = "Último";
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		$this->data['links']  = $this->pagination->create_links();
		$this->data['pagina'] = $pagina;
		$this->data['pagina_pendiente'] = 1;
		$this->data['pagina_revisado']  = 1;
		$this->data['pagina_revision']  = 1;
		$this->data['pagina_urgente']   = 1;
		//$this->data['results'] = $this->erian->fetch();
		$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'*',null,null,'id');
		$this->template->title = "Brigada » ayuda";
		$this->data['estados'] = _estados();
		
		$this->template->content->view('inicio',$this->data);
		$this->template->publish('template_site');

	}

	/**
	 * obtiene información para actualización de campo estado
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function getData($id,$estado,$pwd){
		if ( empty($id) || empty($estado) )
			redirect('inicio/index');
		if( !_confirmacion($pwd) )
			redirect('inicio/index');
		$fields = array('estado'=>array('value'=>$this->security->xss_clean($estado)));
		$result = $this->erian->update($fields,$id);
		
		redirect('inicio/index');
	}//end function

	/**
	 * Función encargada de mostrar vista de las brigadas
	 * @return [type] [description]
	 */
	public function brigadas(){

		$this->template->content->view('brigadas',$this->data);
		$this->template->publish('template_site');
	}





	public function filtro($filtro='',$pagina=1){

		$config['base_url']         = site_url("inicio/filtro/$filtro");
		$estado = _getEstado($filtro);
		$config['total_rows']       = ( !empty($estado) ) ? $this->erian->countRecords($estado) : $this->erian->countRecords();
		$config['per_page']         =  10;
		$config['cur_tag_open']     = '<a class="active">';
		$config['cur_tag_close']    = '</a>';
		$config['first_link']       = "Primero";
		$config['last_link']        = "Último";
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		$this->data['links']     = $this->pagination->create_links();
		

		switch ($filtro) {
			case 'pendientes': 
				$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'*',array('estado'=>1),null,'id');
				$this->data['pagina_pendiente'] = $pagina;
				break;
			case 'revisado':
				$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'*',array('estado'=>2),null,'id');
				$this->data['pagina_revisado'] = $pagina;
				break;
			case 'requiere_revision':
				$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'*',array('estado'=>3),null,'id');
				$this->data['pagina_revision'] = $pagina;
				break;
			case 'urgente':
				$this->data['results'] = $this->erian->paginacion_elements($config['per_page'],$pagina,'*',array('estado'=>4),null,'id');
				$this->data['pagina_urgente'] = $pagina;
				break;
			default:
				redirect('inicio');
				break;
		}

		//$this->data['results'] = $this->erian->fetch();
		
		$this->template->title = "Brigada » ayuda";
		$this->data['estados'] = _estados();
		$this->template->content->view('inicio',$this->data);
		$this->template->publish('template_site');
	}

}//end class
