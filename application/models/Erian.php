<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*   Libreria ERIAN
*       Creada por Víctor Manuel Cruz Romero
*       Año de Creación: 2015
*
* NOTA:
*       ESTA ES SÓLO UNA COPIA DE LA LIBRERIA ORIGINAL
*
*/

class Erian extends CI_Model {

	var $table = "tab_ubicaciones";
	
	var $id = "id";


	/*
	*Funcion encargada de iniciar la sesion
	*
	*/
	public function getLogin($usuario=null){
		if ( !is_array($usuario) || is_null($usuario))
			return false;

		$this->db->select('id,nombre,alias,tipo');
        $this->db->where('status',1);
		$this->db->where('password',$usuario['pwd']);
        $this->db->like('alias',$usuario['alias'],'both');
        $this->db->or_like('alias',strtolower($usuario['alias']),'both');
        $this->db->or_like('alias',ucfirst($usuario['alias']).'both');
        
		$query = $this->db->get($this->table);
        log_message('info','====>GetLogin===>'.$this->db->last_query());
		if ($query->num_rows() >= 1)
			return $query->result();
		else
			return false;
	}//end function


	/*
	* Funcion generica para consulta de tablas simple
	* @params $filtro - arreglo con los campos y valores para el filtro de la consulta
	* @params $select - cadena separada por coma (,) para seleccionar los campos de la consulta
	*/
	public function get( $filro = null, $select = "*" ){


		if( $filro != null && is_array($filro) )
			foreach( $filro as $key=>$data )
				$this->db->where($key,$data);

			$query = $this->db->select($select,false);
			$query = $this->db->get($this->table);

			if ($query->num_rows() >= 1)
				return $query->result();
			else
				return false;
	}//end function


	/*
	* Agrega un nuevo registro
	* @params $cliente arreglo con la información del cliente
	*/
	public function add( $registro = null ){


		if( $registro != null && is_array($registro) ){
			foreach( $registro as $key => $info )
				$data[$key] = $info['value'];
			return $this->db->insert($this->table, $data);
		}
	return false;
	}//end function



	/*
	* Edita un registro
	* @params $fields arreglo con la información del cliente
	* @params $id id del registro para modificar
	*/
	public function update( $fields = null, $id = '' ){

		if ( !is_numeric($id) )
			return false;

		foreach ($fields as $key => $info) 
			$data[$key] = $this->security->xss_clean($fields[$key]['value']);

		$this->db->where('id',$id);
		return $this->db->update($this->table,$data);
	}//end function


	/**
	* Funcion encargada de eliminar un registro, (borrado logico)
	* @param  string $table
	* @param  int $id
	* @return boolean        true/false
	*/
	public function delete( $id='' ){
		if ( !is_numeric($id))
			return false;
		$this->db->where('id',$id);
		return $this->db->update($this->table,array('status'=>-1));
	}//end function


	/*
	*cuenta los registros de la tabla
	*/
	public function countRecords($filtro=''){
		if(!empty($filtro))
			$this->db->where("estado",$filtro);	
		$this->db->where("status!=",-1);
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}//end function


	public function fetch($filro = null, $select = "*" ){
		if( $filro != null && is_array($filro) )
			foreach( $filro as $key=>$data )
				$this->db->where($key,$data);

			$this->db->select($select,false);
			$this->db->order_by('id','desc');

			$query = $this->db->get($this->table);

			if ($query->num_rows() >= 1)
				return $query->result();
			else
				return false;
	}

	public function fetchDistancia($latitud, $longitud) {
	 	$resultado['success'] = 0;
	    $resultado['extra'] = "La consulta no genero resultados";
	    $this->table = 'tab_ubicaciones';
	    $raw_query = 'SELECT resultado.*
	    FROM (
	      SELECT *, (acos(sin(radians('.$latitud.')) * sin(radians(latitud)) + cos(radians('.$latitud.')) * cos(radians(latitud)) * cos(radians('.$longitud.') - radians(longitud))) * 6378) AS distancia
	      FROM '.$this->table.'
	      WHERE status = 1
	    ) resultado
	    ORDER BY resultado.distancia ASC';
	    $query = $this->db->query($raw_query);
	    if ($query->num_rows() > 0){
	      $resultado['success'] = 1;
	      $resultado['extra'] = $query->result();
	    }
	    return $resultado;
	} 

  /**
   * Funcion que obtiiene datos para paginacion
   * @param  integer $per_page [description]
   * @param  integer $pagina   [description]
   * @param  string  $select   [description]
   * @param  [type]  $filtro   [description]
   * @param  [type]  $join     [description]
   * @param  string  $order_id [description]
   * @return [type]            [description]
   */
    public function paginacion_elements($per_page=10,$pagina=1,$select='*',$filtro=null,$join=null,$order_id=''){

        
        $this->db->limit($per_page, ( ($pagina*$per_page)-$per_page ) );
        $this->db->select($select,false);
        $this->db->from($this->table);
        if ($join!=null && is_array($join)) {
            $this->db->join($join['table'],$join['cond']);
            $this->db->where($this->table.'.status',1);
        }else
            $this->db->where('status!=',-1);
        if( $filtro != null && is_array($filtro) )
          foreach( $filtro as $key=>$data )
            $this->db->where($key,$data);
        
        $this->db->order_by($order_id,'DESC');
        $query = $this->db->get();
        $data = array();
        if($query->num_rows() > 0){
            foreach($query->result() as $fila)
                $data[] = $fila;
            return $data;
        }
        return false;
    }//end functi

    /**
     * Funcion encargada de validar si existen duplicados del campo random_id 
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function duplicadosID($random_id=''){
    /*SELECT random_id, COUNT(*) Total
    FROM tab_ubicaciones
    GROUP BY random_id
    HAVING COUNT(*) > 1*/
        $this->db->select('random_id, count(*) total',false);
        $this->db->from('tab_ubicaciones');
        $this->db->where('random_id',$random_id);
        $this->db->group_by('random_id');
        $this->db->having('count(*)>1');
        $query = $this->db->get();
        log_message('info','===>Query: '.$this->db->last_query());
        if($query->num_rows() > 0){
            log_message('info','=========>RETURN true: '.$query->num_rows());
            return true;
        }
        else{
            log_message('info','=========>RETURN false: '.$query->num_rows());
            return false;
        }
    
    }
}
?>
