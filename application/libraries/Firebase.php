<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
define('FIREBASE_API_KEY','AIzaSyAkgCmyLVqheGdMpqUtM_yPb5Q7kjLooSQ');
define('PROJECT_ID', 'cicluz-3fc62');
/**
 *
 */
class Firebase{

	public function send($dispositivo, $mensaje) {
		$fields = array(
			'to' => $dispositivo,
			'data' => $this->cuerpoMensaje($mensaje)
		);
		return $this->sendPushNotification($fields);
	}

	public function sendChofer($dispositivo, $mensaje) {
		$fields = array(
			'to' => $dispositivo,
			'data' => $this->mensajeChofer($mensaje)
		);
		return $this->sendPushNotification($fields);
	}


	public function sendMultiple($dispositivos, $mensaje) {
		$respuesta = array(
			'success' => 0,
			'failure' => 0
		);
		log_message('info','===>sendMultiple===>'.json_encode($dispositivos));
		log_message('info','===>sendMultiple===>'.json_encode($mensaje));
		if (sizeof($dispositivos) > 0) {
			foreach ($dispositivos as $dispositivo) {
				if(!empty($dispositivo->firebaseID)){
					log_message('info','===>sendMultiple===>'.$dispositivo->firebaseID);
					$fields = array(
						'to' => $dispositivo->firebaseID,
						"priority" => "normal",
						'time_to_live' => 900,
						'data' => $this->cuerpoMensaje($mensaje)
					);
					$respuestas = $this->sendPushNotification($fields);
					log_message('info','===>respuestaFirebase===>'.json_encode($respuestas));
					$resultado = json_decode($respuestas);
					$respuesta['success'] = $respuesta['success'] + $resultado->success;
					$respuesta['failure'] = $respuesta['failure'] + $resultado->failure;
					log_message('info','===>sendMultiple===>'.json_encode($respuesta));
				}
			// log_message('info', $respuestas);

        }
      }
      return $respuesta;
  	}

	private function cuerpoMensaje($value = null) {
		/*
		$extra['imageUrl'] = $value['imageUrl'];
		$extra['longitud'] = $value['longitud'];
		$extra['latitud'] = $value['latitud'];
		$extra['ordenReg'] = (array_key_exists('ordenReg',$value))?$value['ordenReg']:'';
		$extra['modo'] = (array_key_exists('modo',$value))?$value['modo']:'';
		*/
		/*$cuerpoNotificacion['tipo'] = $value['tipo'];*/
		$cuerpoNotificacion['titulo'] = $value['titulo'];
		$cuerpoNotificacion['mensaje'] = $value['mensaje'];
		$cuerpoNotificacion['timestamp'] = $value['timestamp'];
		$cuerpoNotificacion['url'] = $value['url'];
		/*$cuerpoNotificacion['extra'] = $extra;*/
		$res['data'] = $cuerpoNotificacion;
		//log_message("info","resFirebase:".$res);
		return $res;
	}


	private function sendPushNotification($fields) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            //'Authorization: key=AIzaSyAkgCmyLVqheGdMpqUtM_yPb5Q7kjLooSQ',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        return $result;
 	}
}//end class
