<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
* Genera limpia el string para usarlo como url
*/
function _slug( $str = null ){

	$characters = array(
		"Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
		"á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
		"à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u", "¿" => "", "?" => "" , "," => "",
		"¡" => "", "!" => "", "'" => "", "%" => "", '"' => ""
		);

	$str = trim(str_replace(' ', '-', $str));
	$str = strtr($str, $characters);

	return strtolower($str);
}

function _confirmacion($pwd=''){
	$clave = 'CCICM2017';
	return ( strcmp($clave,$pwd) == 0 ) ? true:false;
}


function _estados(){
	$estados = array(
		'1'=>'Pendiente',
		'2'=>'Revisado',
		'3'=>'Requiere revisión física',
		'4'=>'Urgente revisión'
	);
	return $estados;
}

function _getEstado($status=''){
	$estado = '';
	switch ($status) {
		case 'pendientes'       :$estado = 1;break;
		case 'revisado'         :$estado = 2;break;
		case 'requiere_revision':$estado = 3;break;
		case 'urgente'          :$estado = 4;break;
		default                 :$estado = '';break;
	}
	return $estado;
}

function _perfiles($user_type){
	$type = '';
	switch ($user_type) {
		case 1:$type = 'Administrador';break;
		case 2:$type = 'Brigadista';break;
		case 3:$type = 'Publico';break;
		default:$ype = 'Publico';break;
	}
	return $type;
}




function _tipoUsuario($tipo=''){
	$type = '';
	switch ($tipo) {
		case 1: $type = 'Administrador';break;
		case 2: $type = 'Usuario';break;
		default:$type = 'Usuario';break;
	}
	return $type;
}


/*
* Envio de mail generico
*/
function _send_mail( $subject = null, $to = null, $body = null ){
	$CI =& get_instance();
	$CI->load->library(array('email'));

	$config = array(
		'mailtype' => 'html',
		'charset' => 'utf-8',
		'newline' => "\r\n"
	);
	$CI->email->initialize($config);

	$CI->email->subject($subject);

	$CI->email->from('no-reply@criptexfc.com', "Criptexfc");
	$CI->email->to($to);

	$CI->email->message($body);

	$CI->email->send();
}


/*
*Funcion encargada de verificar el formato de la imagen y tamaño
*return true on success
*/
function _verificaImagen($extension,$size){
	if (strcmp("jpg",$extension)== 0 || strcmp('png',$extension)== 0 || strcmp('jpge',$extension)== 0 || strcmp('gif',$extension)==0) {
		return true;
	}else
		return false;
}//end function


/*
* Cadena de caracteres aleatorios
*/
function _RandomString($length=11,$uc=TRUE,$n=TRUE,$sc=FALSE){
	$source = 'abcdefghijklmnopqrstuvwxyz';
	if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	if($n==1) $source .= '1234567890';
	if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
	if($length>0){
		$rstr = "";
		$source = str_split($source,1);
		for($i=1; $i<=$length; $i++){
			mt_srand((double)microtime() * 100000);
			$num = mt_rand(1,count($source));
			$rstr .= $source[$num-1];
		}

	}
	return $rstr;
}