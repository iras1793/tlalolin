<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/public/css/app.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/public/css/bootstrap-select.css'); ?>">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/public/css/smoke.css")?>" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url("assets/public/js/smoke.min.js")?>"></script>
</head>
<body>
<header>
<style type="text/css">
#menu-outer {height: 84px;text-align: center;}
.table {display: table;margin: 0 auto;}
ul#horizontal-list {min-width: 696px;list-style: none;padding-top: 20px;}
ul#horizontal-list li {display: inline;}
</style>
<div id="menu-outer">
	<div class="table">
		<ul id="horizontal-list">
			<li><a href="<?php echo site_url("inicio/filtro/pendientes/$pagina_pendiente") ?>"><img src="<?php echo base_url('assets/public/img/icons/revision.png'); ?>" alt="Pendientes" />Pendientes</a></li>&nbsp;&nbsp;&nbsp;
			<li><a href="<?php echo site_url("inicio/filtro/revisado/$pagina_revisado") ?>"><img src="<?php echo base_url('assets/public/img/icons/checked.png'); ?>" alt="Revisado" />Revisado</a></li>&nbsp;&nbsp;&nbsp;
			<li><a href="<?php echo site_url("inicio/filtro/requiere_revision/$pagina_revision") ?>"><img src="<?php echo base_url('assets/public/img/icons/warning.png'); ?>" alt="Requiere revisión Física" />Requiere revisión Física</a></li>&nbsp;&nbsp;&nbsp;
			<li><a href="<?php echo site_url("inicio/filtro/urgente/$pagina_urgente") ?>"><img src="<?php echo base_url('assets/public/img/icons/alert.png'); ?>" alt="Urgente revisión" />Urgente revisión</a></li>
		</ul>
	</div>
</div>
</header>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<?php echo $content; ?>
<div id="cont_pager">
<?php echo $links; ?>
</div>
<script src="<?php echo base_url('assets/public/js/app.js'); ?>"></script>
<script src="<?php echo base_url('assets/public/js/bootstrap-select.js'); ?>"></script>
	


</body>
</html>