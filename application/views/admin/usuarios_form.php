<section class="wrapper">
    <h1 id="title"><?php echo $this->template->title ?></h1>
    <div class="flexContent">
<?php
  echo validation_errors('<div class="error">', '</div>');
  
    $all = $this->messages->get();
    foreach($all as $type=>$messages)
        foreach($messages as $message)
          echo '<div class="'.$type.'">'.$message.'</div>';         
?>
    <a href="<?php echo site_url('admin/inicio'); ?>" class="btn">
      <span class="txt_btn">Regresar</span>
      <span class="img_btn">
        <img src="<?php echo base_url('assets/admin/img/back.png'); ?>" alt="Regresar" width="28px"/>
      </span>
    </a>
      <div class="txtRight">
      </div>
<?php echo form_open("admin/inicio/$action/$id") ?>
  <br><br>
      <table class="table_edit txtSiz14">
        <thead>
            <tr>
                <th colspan="2" align="center"><?php echo $titulo ?></th>
            </tr>
        </thead>
          <tr>
            <td width="30px"><label>Nombre</label></td>
            <td>
              <input type="text" name="nombre" value="<?php echo $fields['nombre']['value'] ?>" class="largeInput">
            </td>
          </tr>
          <tr>
            <td width="30px"><label>Alias</label></td>
            <td>
              <input type="text" name="alias" value="<?php echo $fields['alias']['value'] ?>" class="largeInput">
            </td>
          </tr>
          <tr>
            <td width="260"><label>Contraseña</label></td>
            <td>
              <input type="password" name="password" value="<?php echo $fields['password']['value'] ?>" class="largeInput">
            </td>
          </tr>
          <tr>
            <td width="260"><label>Tipo de usuario</label></td>
            <td>
                <select name="tipo">
                <option value="">Seleccione el tipo</option>
                <?php foreach ($tipos as $index => $tipo): ?>
                    <?php if (strcmp($fields['tipo']['value'],$index)==0): ?>
                        <option value="<?php echo $index ?>" selected><?php echo $tipo ?></option>
                        <?php else: ?>
                            <option value="<?php echo $index ?>"><?php echo $tipo ?></option>
                    <?php endif ?>
                <?php endforeach ?>
                    
                </select>              
            </td>
          </tr>

      </table>
      <div class="agregar">
          <a href="<?php echo site_url('admin/inicio'); ?>" class="btn">
            <span class="txt_btn">Cancelar</span>
            <span class="img_btn">
              <img src="<?php echo base_url('assets/admin/img/cancel.png'); ?>" alt="Cancelar" width="28px" />
            </span>
          </a>
          <input type="submit" value="Guardar">
      </div>
<?php echo form_close() ?>




</div>
