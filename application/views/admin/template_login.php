<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tlalolin::Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url('assets/admin/img/favicon.png') ?>" type="image/x-icon">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/normalize.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/main.css') ?>">
    <script src="<?php echo base_url('assets/admin/js/vendor/modernizr-2.8.3.min.js') ?>"></script>
    <script type="text/javascript">
        WebFontConfig = {
        google: { families: [ 'Open+Sans:400,600,700,800,300:latin' ] }
        };
        (function() {
        var wf = document.createElement('script');
        /*wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';*/
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
        })();
    </script>
</head>
<body>

<?php  

echo $content;

?>


<footer>
<br><br><br><br><br><br>
  <p class="txt txtSiz12">
    Desarrollado por:
  </p>
  Víctor Cruz
</footer>

<div id="cont_recupera">
    <span id="cerrar_recupera"><img src="img/ico_cancelGray.png" alt="Cerrar" /></span>
    <p class="txt txtSiz14">
        Escribe el mail con el que estás registrado.
        <br>
        Te llegará un mail con tu contraseña.
    </p>
    <input type="text" name="name" value="" placeholder="Email">
    <input type="text" name="name" value="" placeholder="Confirma tu email">
    <input type="submit" name="name" value="Enviar">
</div>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/admin/js/vendor/jquery-1.12.0.min.js') ?>"><\/script>')</script>
<script src="<?php echo base_url('assets/admin/js/plugins.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/js/main.js') ?>"></script>
</body>
</html>
