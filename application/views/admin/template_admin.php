<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tlalolin::Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url('assets/admin/img/favicon.png') ?>" type="image/x-icon">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/normalize.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/main.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/admin/css/smoke.css")?>" type="text/css" />
    <script src="<?php echo base_url('assets/admin/js/vendor/modernizr-2.8.3.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/admin/js/smoke.min.js")?>"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <script type="text/javascript">
        WebFontConfig = {
        google: { families: [ 'Open+Sans:400,600,700,800,300:latin' ] }
        };
        (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
        })();
    </script>
</head>
<body>





<?php  

echo $content;

?>

<?php if(isset($links)): ?>
<div id="cont_pager">
    <?php echo $links; ?>   
</div>
<?php endif; ?>
</body>
<br><br><br><br><br><br>
<footer>
  <p class="txt txtSiz12">
    Desarrollado por:
  </p>
Alumnos de Universidad Autónoma Metropilitana junto con El Colegio de Ingenieros Civiles de México (CICM) 
</footer>

<nav>
  <div id="logoIntMenu">
    <img src="<?php echo base_url('assets/admin/img/uam-ico2.png') ?>" alt="logo" width="90px" height="90px"/>
  </div>
  <ul class="txtSiz14">
    <li><a href ="<?php echo site_url('admin/inicio') ?>" class="<?php if(strcmp($this->template->title,'Usuarios')==0) echo 'active'; ?>">Usuarios</a></li>
  </ul>
  <div id="btn_salir">
    <a href="<?php echo site_url('admin/login/logout') ?>">Salir</a>
    <br><br>
    <a href="<?php echo site_url('admin/login/logout'); ?>">
      <svg width="36px" height="44px" viewBox="0 0 36 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="ico_apaga" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="Inicio" transform="translate(-36.000000, -910.000000)">
                  <g id="ico_shutdown" transform="translate(36.000000, 910.000000)">
                      <g id="Group">
                          <path d="M27.026087,10.4761905 L26.1913043,11.9428571 C31.1478261,14.8238095 34.226087,20.1666667 34.226087,25.8761905 C34.226087,34.7809524 27.026087,42.0095238 18.1565217,42.0095238 C9.28695652,42.0095238 2.08695652,34.7809524 2.08695652,25.8761905 C2.08695652,20.1666667 5.16521739,14.8238095 10.1217391,11.9428571 L9.28695652,10.4761905 C3.80869565,13.6714286 0.417391304,19.5380952 0.417391304,25.8761905 C0.417391304,35.6714286 8.4,43.6857143 18.1565217,43.6857143 C27.9130435,43.6857143 35.8956522,35.7238095 35.8956522,25.8761905 C35.8956522,19.5380952 32.5043478,13.6190476 27.026087,10.4761905 L27.026087,10.4761905 Z" id="Shape"></path>
                          <rect id="Rectangle-path" x="17.3217391" y="0.419047619" width="1.66956522" height="16.9714286"></rect>
                      </g>
                  </g>
              </g>
          </g>
      </svg>
    </a>
  </div>
</nav>


<!-- <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script> -->
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/admin/js/vendor/jquery-1.12.0.min.js') ?>"><\/script>')</script>
<script src="<?php echo base_url('assets/admin/js/plugins.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/js/main.js') ?>"></script>

</html>
