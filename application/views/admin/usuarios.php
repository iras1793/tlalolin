<section class="wrapper">
    <h1 id="title">Administrador tlalolin<br><?php echo $this->template->title ?></h1>
    <div class="flexContent">
<?php

  echo validation_errors('<div class="error">', '</div>');
  
    $all = $this->messages->get();
    foreach($all as $type=>$messages)
        foreach($messages as $message)
          echo '<div class="'.$type.'">'.$message.'</div>';         
?>

       <div class="txtRight">
          <!--<div class="buscador"> -->
          <?php #echo form_open('inicio/buscador'); ?>
            <!-- <input type="search" name="buscar" placeholder="Buscar por nombre" onpaste="return false" name="restaurantes" id="restaurantes" class="restaurantes" autocomplete="off" >       -->
            
            <!-- <div class="muestra_restaurantes"></div> -->
          <?php #echo form_submit('buscar','Buscar') ?>
          <!-- <input type="submit" name="Buscar" value="Buscar"> -->
          <?php #echo form_close() ?>
<!--           </div> -->
         <a href="<?php echo site_url('admin/inicio/agregar') ?>" class="btn" name="tip">
            <span class="txt_btn">Agregar usuario</span>
            <span class="img_btn">
                <img src="<?php echo base_url('assets/admin/img/plus.png') ?>" alt="eliminar" /> 
             </span>
        </a>
      </div> 

      <table class="table_items txtSiz14">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Alias</th>
              <th>Rol</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
  <?php if (is_array($results)):
            foreach ($results as $info):
  ?>
          <tr>
            <td align="center"><?php echo $info->nombre ?></td>
            <td align="center"><?php echo $info->alias ?></td>
            <td align="center"><?php echo _tipoUsuario($info->tipo) ?></td>
            <td width="30"><a href="<?php echo site_url('admin/inicio/editar/').$info->id;?>"><img src="<?php echo base_url('assets/admin/img/edit.png'); ?>" alt="" /></a></td>
            <td width="30"><a href="javascript:void(0)"  onclick="eliminar(<?php echo $info->id ?>);" ><img src="<?php echo base_url('assets/admin/img/delete.png') ?>"/></a></td>
          </tr>
<?php 
    endforeach;
endif; ?>
      </table>

</div>

<script type="text/javascript">
    function eliminar( id ){
    smoke.confirm("¿Desea eliminar el registro?", function(e){
      if (e){
        javascript:window.location='<?php echo site_url("admin/inicio/eliminar/") ?>/'+id
      }else{
        javascript:window.location='<?php echo site_url("admin/inicio") ?>'
      }
    }, {
      ok: "Si",
      cancel: "No",
      reverseButtons: true
    });
    return true;
  }
</script>
