<?php 

 ?>
<div id="carousel" class="container-fluid carousel slide carousel-fade image-background" data-interval="false">
	<?php #start ?>
	<div class="row table-reports">
		<?php if ( is_array($results) && count($results) > 0): ?>
			<?php foreach ($results as $index => $info): ?>
				<div class="col-md-12">
					<div class="report">
						<div class="image-background image" style="background-image: url(<?php echo base_url('img/').$info->foto ?>);width: 500px;height: 350px;"></div>
						<p><img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/23.svg') ?>">Nombre de contacto: <strong><?php echo $info->contacto ?></strong></p>
						<p><img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/26.svg') ?>">Teléfono: <strong><?php echo $info->telefono ?></strong></p>
						<p><img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/09.svg') ?>">Observaciones del lugar: <strong><?php echo $info->observaciones ?></strong></p>
						<p><img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/08.svg'); ?>">Latitud: <strong><?php echo $info->latitud ?></strong></p>
						<p><img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/08.svg'); ?>">Longitud: <strong><?php echo $info->longitud ?></strong></p>
						<p>
							<img class="svg icon-title" src="<?php echo base_url('assets/public/img/icons/warning.svg'); ?>">Estado:
						
						<select onchange="cambiaEstado( this , <?php echo $info->id ?>);">
							<option value="">Sin estado</option>
							<?php foreach ($estados as $index => $estado): ?>
								<?php if ( strcmp($index,$info->estado) == 0): ?>
									<option value="<?php echo $index ?>" selected><?php echo $estado ?></option>
									<?php else: ?>
										<option value="<?php echo $index ?>"><?php echo $estado ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
						</p>
					</div>
				</div>
			<?php endforeach ?>
		<?php else: ?>
			<h1 align="center">Aún sin actualizaciónes de éste estado. </h1>
		<?php endif ?>


	<?php #finish ?>
	</div>
	<?php #para qué es esta linea???? ?>
		<!-- <div class="row table-reports" data-fn="contacts" data-url="https://codepen.io/nakome/pen/DnEvr.js"></div> -->
</div>

<script type="text/javascript">

	function cambiaEstado(select,instance) {
		
		var opcion = select.options[select.selectedIndex];
		smoke.prompt('Para cambiar el estado ingresa la contraseña',function(e){
			if (e.localeCompare('')!=0){
				javascript:window.location='<?php echo site_url("inicio/getData/") ?>'+instance+'/'+opcion.value+'/'+e
			}else{
				javascript:window.location='<?php echo site_url("inicio/") ?>'
			}
		}, {
			ok: "Aceptar",
			cancel: "Cancelar",
			classname: "custom-class",
			reverseButtons: true,
			value: ""
		});
		return true;
	}
</script>